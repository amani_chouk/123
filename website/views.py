
from django.http import request
from django.shortcuts import redirect, render
from immo.models import Property
from django.contrib.auth.models import User
from website.models import UserProfile
import re
from django.contrib import messages
from django.contrib import auth
# Create your views here.

def dashboard(request):
    if request.method == 'POST' and 'btnsave' in request.POST:
        if request.user is not None and request.user.id != None:
           userprofile = UserProfile.objects.get(user=request.user)

           if request.POST['fname'] and request.POST['num'] and request.POST['email']and request.POST['title'] and request.POST['city']and request.POST['state']and request.POST['address'] and request.POST['zip'] and request.POST['about'] and request.POST['face'] and request.POST['google'] and request.POST['twitter'] and request.POST['linkedin'] :
               request.user.email = request.POST['email']
               userprofile.num = request.POST['num']
               userprofile.full_name = request.POST['fname']
               userprofile.title = request.POST['title']
               userprofile.city = request.POST['city']
               userprofile.state = request.POST['state']
               userprofile.zip = request.POST['zip']
               userprofile.about = request.POST['about']
               userprofile.address = request.POST['address']
               userprofile.facebook = request.POST['face']
               userprofile.twitter = request.POST['twitter']
               userprofile.google = request.POST['google']
               userprofile.linkedin = request.POST['linkedin']

               request.user.save()
               userprofile.save()
               messages.success(request, 'your data has been saved')
           else:
                   messages.error(request, 'check your values and elements')
           return redirect ('myprofil')
    else:
        #dkhal maghir mayinzil ala chy .. affichage ..save detail
        if request.user is not None:
            context = None
            if not request.user.is_anonymous:

              userprofile = UserProfile.objects.get(user=request.user)
              context = {
                'fname':userprofile.full_name,
                'num':userprofile.num,
                'address':userprofile.address,
                'email':request.user.email,
                'city':userprofile.city,
                'state':userprofile.state,
                'title':userprofile.title,
                'zip':userprofile.zip,
                'about':userprofile.about,
                'face':userprofile.facebook,
                'twitter':userprofile.twitter,
                'google':userprofile.google,
                'linkedin':userprofile.linkedin,

                
             
             
                  }
            return render (request, 'accounts/dashboard.html', context)
        else:
            return redirect('dashboard')
    return render (request, 'accounts/dashboard.html')

def bookmark(request):
    return render (request, 'accounts/bookmark.html')
def submit(request):
     if request.method == 'POST' and 'btnsumb' in request.POST :
         
           savedata = Property()
           if request.POST['Property_Title'] and request.POST['statuss'] and request.POST['price'] and request.POST['area'] and request.POST['bedrooms'] and request.POST['bathrooms'] and request.POST['location'] and request.POST['type1'] and request.POST['city'] and request.POST['state'] and request.POST['Zip_Code'] and request.POST['Name'] and request.POST['Email'] and request.POST['phone'] and request.POST['Rooms'] and request.POST['garage'] and request.POST['building_age'] and request.POST['description']:
            
             savedata.Property_Title=request.POST.get('Property_Title')
             
             savedata.statuss=request.POST['statuss']
             savedata.price=request.POST['price']
             savedata.area=request.POST['area']
             savedata.bedrooms=request.POST['bedrooms']
             savedata.bathrooms=request.POST['bathrooms']
             savedata.location=request.POST['location']
             savedata.type1=request.POST['type1']
             savedata.city=request.POST['city']
             savedata.state=request.POST['state']
             savedata.Zip_Code=request.POST['Zip_Code']
            
             savedata.Name=request.POST['Name']
             savedata.Email=request.POST['Email']
             savedata.phone=request.POST['phone']
             savedata.description=request.POST['description']
             savedata.building_age=request.POST['building_age']
             savedata.garage=request.POST['garage']
             savedata.Rooms=request.POST['Rooms']
             savedata.save()
             messages.success(request, 'your data has been saved')
           else:
                   messages.error(request, 'check your values and elements')
           return redirect ('submit')
         
     else:
            return render (request, 'accounts/submit.html')
    
def my_prop(request):
    return render (request, 'accounts/my_prop.html')

def signin(request):
    if request.method == "POST" and 'Login' in request.POST:
        
         username = request.POST['user']
         password = request.POST['pass']
         user = auth.authenticate(username=username, password=password)
         if user is not None:
          auth.login(request, user)
          return redirect('myprofil')
         else:
            messages.error(request, 'username or password invalid') 
         return redirect('signin')
    
    
    else:          
      return render (request, 'accounts/signin.html')
def logout(request):
    if request.user.is_authenticated:
        auth.logout(request)
    return redirect('index')
def signup(request):
    if request.method == "POST" and 'signup' in request.POST:
        
         
         
         fname=None
         email = None
         username = None
         num = None
         password = None
         type = None
         is_added = None  
         if 'fname' in request.POST: fname = request.POST['fname']
         else: messages.error(request,'Error in Full name')               
         if 'email' in request.POST: email = request.POST['email']
         else: messages.error(request,'Error in Email')
         if 'user' in request.POST: username = request.POST['user']
         else: messages.error(request,'Error in username')
         if 'num' in request.POST: num = request.POST['num']
         else: messages.error(request,'Error in your phone')
         if 'pass' in request.POST: password = request.POST['pass']
         else: messages.error(request,'Error in your password')
         if 'type' in request.POST: type = request.POST['type']
         if fname and username and email and password and num  and type:
             
                if User.objects.filter(username=username).exists():
                    messages.error(request,'this username is taken')
                else:
                    if User.objects.filter(email=email).exists():
                        messages.error(request, 'this email is taken')
                    else:
                        patt = "^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$"
                        if re.match(patt, email):
                            #add user
                            user=User.objects.create_user(email=email,password=password,username=username)
                            user.save()
                            #add user profile
                            userprofile =UserProfile(user=user, full_name=fname, num=num, type=type)
                            userprofile.save()
                            #clear fields
                            fname=''
                            email=''
                            username=''
                            num=''
                            password =''
                            type=''
                            messages.success(request, 'your accounts is created')
                            is_added = True
                        else:
                            messages.error(request, 'unvalid email')
             
             
             
         else:
             messages.error(request, 'check empty fields')
        
         return render(request, 'accounts/signup.html', {
             'fname':fname,
             'email' : email,
             'user' : username,
             'num' : num,
             'pass' : password,
             'type' : type ,
             'is_added': is_added,
         })
    else:


        
    
       return render (request, 'accounts/signup.html')
def myprofil(request):
    if request.method == 'POST' and 'btnsave' in request.POST:
        if request.user is not None and request.user.id != None:
           userprofile = UserProfile.objects.get(user=request.user)

           if request.POST['fname'] and request.POST['num'] and request.POST['email']and request.POST['title'] and request.POST['city']and request.POST['state']and request.POST['address'] and request.POST['zip'] and request.POST['about'] and request.POST['face'] and request.POST['google'] and request.POST['twitter'] and request.POST['linkedin'] :
               request.user.email = request.POST['email']
               userprofile.num = request.POST['num']
               userprofile.full_name = request.POST['fname']
               userprofile.title = request.POST['title']
               userprofile.city = request.POST['city']
               userprofile.state = request.POST['state']
               userprofile.zip = request.POST['zip']
               userprofile.about = request.POST['about']
               userprofile.address = request.POST['address']
               userprofile.facebook = request.POST['face']
               userprofile.twitter = request.POST['twitter']
               userprofile.google = request.POST['google']
               userprofile.linkedin = request.POST['linkedin']

               request.user.save()
               userprofile.save()
               messages.success(request, 'your data has been saved')
           else:
                   messages.error(request, 'check your values and elements')
           return redirect ('myprofil')
    else:
        #dkhal maghir mayinzil ala chy .. affichage ..save detail
        if request.user is not None:
            context = None
            if not request.user.is_anonymous:

              userprofile = UserProfile.objects.get(user=request.user)
              context = {
                'fname':userprofile.full_name,
                'num':userprofile.num,
                'address':userprofile.address,
                'email':request.user.email,
                'city':userprofile.city,
                'state':userprofile.state,
                'title':userprofile.title,
                'zip':userprofile.zip,
                'about':userprofile.about,
                'face':userprofile.facebook,
                'twitter':userprofile.twitter,
                'google':userprofile.google,
                'linkedin':userprofile.linkedin,

                
             
             
                  }
            return render (request, 'accounts/myprofil.html', context)
        else:
            return redirect('myprofil')
 
 
def product_favorite(request,ann_id):
    if request.user.is_authenticated and not request.user.is_anonymous:
        pro_fav = Property.objects.get(pk=ann_id)
        if  UserProfile.objects.filter(user=request.user, product_favorites=pro_fav).exists():
            messages.success(request, 'Already product in the favorite list')
        else:
            userprofile = UserProfile.objects.get(user = request.user)
            userprofile.product_favorites.add(pro_fav)
            messages.success(request, 'product has been favorited')
        
    else:
        messages.error(request, 'you must be logged in')
 
    return redirect('/immo/' + str(ann_id))
def show_product_favorite(request):
    context=None
    if request.user.is_authenticated and not request.user.is_anonymous:
        userInfo = UserProfile.objects.get(user=request.user)
        ann = userInfo.product_favorites.all()
        context = {'annonces':ann}
    return render(request, 'immo/favorites.html', context )