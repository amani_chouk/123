from django.urls import path
from . import views
from django.contrib.auth import views as auth_views
urlpatterns = [
    path( 'myprofil',views.myprofil, name = 'myprofil' ),
    path( 'dashboard',views.dashboard, name = 'dashboard' ),
    path( 'change_passwrd',auth_views.PasswordChangeView.as_view(template_name='accounts/change_passwrd.html'), name = 'change_passwrd' ),
    path( 'password_change_done',auth_views.PasswordChangeDoneView.as_view(template_name='accounts/change_password_done.html'), name = 'password_change_done' ),
    path( 'bookmark',views.bookmark, name = 'bookmark' ),
    path( 'submit',views.submit, name = 'submit' ),
    path( 'my_prop',views.my_prop, name = 'my_prop' ),
    path( 'signin',views.signin, name = 'signin' ),
    path( 'signup',views.signup, name = 'signup' ),
    path('logout', views.logout, name= 'logout'),
    path('product_favorite/<int:ann_id>', views.product_favorite, name= 'product_favorite'),
    path('show_product_favorite', views.show_product_favorite, name= 'show_product_favorite'),
    


]